﻿/*
 *  GridView To GridView
 *  Exercise
 *  Created By : Naing Min Htet Oo
 *  Created Date : 2021/07/07
 */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GridtoGrid_NMHO
{
    public partial class frmGridToGrid : Form
    {
        public frmGridToGrid()
        {
            InitializeComponent();
        }

        private void frmGridToGrid_Load(object sender, EventArgs e)
        {
            // for datagridview1
            // create textbox column
            DataGridViewTextBoxColumn dgvId = new DataGridViewTextBoxColumn();
            dgvId.HeaderText = "Id";
            DataGridViewTextBoxColumn dgvFn = new DataGridViewTextBoxColumn();
            dgvFn.HeaderText = "First Name";
            DataGridViewTextBoxColumn dgvLn = new DataGridViewTextBoxColumn();
            dgvLn.HeaderText = "Last Name";

            // create checkbox column
            DataGridViewCheckBoxColumn dgvCheckBox = new DataGridViewCheckBoxColumn();
            dgvCheckBox.HeaderText = "Select";

            // add column to datagridview
            dataGridView1.Columns.Add(dgvId);
            dataGridView1.Columns.Add(dgvFn);
            dataGridView1.Columns.Add(dgvLn);
            dataGridView1.Columns.Add(dgvCheckBox);

            // add row to datagridview
            dataGridView1.Rows.Add("1", "First Name 1", "Last Name 1", false);
            dataGridView1.Rows.Add("2", "First Name 2", "Last Name 2", false);
            dataGridView1.Rows.Add("3", "First Name 3", "Last Name 3", false);
            dataGridView1.Rows.Add("4", "First Name 4", "Last Name 4", false);
            dataGridView1.Rows.Add("5", "First Name 5", "Last Name 5", false);
            dataGridView1.Rows.Add("6", "First Name 6", "Last Name 6", false);
            dataGridView1.Rows.Add("7", "First Name 7", "Last Name 7", false);
            dataGridView1.Rows.Add("8", "First Name 8", "Last Name 8", false);
            dataGridView1.Rows.Add("9", "First Name 9", "Last Name 9", false);
            dataGridView1.Rows.Add("10", "First Name 10", "Last Name 10", false);
            dataGridView1.Rows.Add("11", "First Name 11", "Last Name 11", false);
            dataGridView1.Rows.Add("12", "First Name 12", "Last Name 12", false);
            dataGridView1.Rows.Add("13", "First Name 13", "Last Name 13", false);
            dataGridView1.Rows.Add("14", "First Name 14", "Last Name 14", false);
            dataGridView1.Rows.Add("14", "First Name 14", "Last Name 14", false);
            dataGridView1.Rows.Add("14", "First Name 14", "Last Name 14", false);
            dataGridView1.Rows.Add("14", "First Name 14", "Last Name 14", false);
            dataGridView1.Rows.Add("14", "First Name 14", "Last Name 14", false);
            dataGridView1.Rows.Add("14", "First Name 14", "Last Name 14", false);
            dataGridView1.Rows.Add("14", "First Name 14", "Last Name 14", false);
            dataGridView1.Rows.Add("14", "First Name 14", "Last Name 14", false);

            // custom row add disabled
            dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dataGridView1.AllowUserToAddRows = false;

            // for datagridview 2
            // create textbox bolumn
            DataGridViewTextBoxColumn dgvId2 = new DataGridViewTextBoxColumn();
            dgvId2.HeaderText = "Id";
            DataGridViewTextBoxColumn dgvFn2 = new DataGridViewTextBoxColumn();
            dgvFn2.HeaderText = "First Name";
            DataGridViewTextBoxColumn dgvLn2 = new DataGridViewTextBoxColumn();
            dgvLn2.HeaderText = "Last Name";

            // add column to datagridview
            dataGridView2.Columns.Add(dgvId2);
            dataGridView2.Columns.Add(dgvFn2);
            dataGridView2.Columns.Add(dgvLn2);

            // custom row add disabled
            dataGridView2.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dataGridView2.AllowUserToAddRows = false;

        }

        private void btnGridToGrid_Click(object sender, EventArgs e)
        {
            for (int i = 0; i <= dataGridView1.Rows.Count - 1; i++)
            {
                // rowAlreadyExist => if the row already exist on datagridview2
                bool rowAlreadyExist = false;
                
                // get value of select column
                bool checkCell = (bool)dataGridView1.Rows[i].Cells[3].Value;

                // check checkbox is selected or not
                if (checkCell)
                {
                    // create datagridview row object
                    DataGridViewRow row = dataGridView1.Rows[i];

                    // check datagridview2 is not empty
                    if(dataGridView2.Rows.Count != 0)
                    {
                        for (int j = 0; j <= dataGridView2.Rows.Count - 1; j++)
                        {
                            // check current data is already exists on datagridview2
                            if (row.Cells[0].Value.ToString() == dataGridView2.Rows[j].Cells[0].Value.ToString())                            
                            {
                                rowAlreadyExist = true;
                                break;
                            }
                        }

                        // check current data is not exists on datagridview2
                        if (!rowAlreadyExist)
                        {
                            // add row to datagridview 2
                            dataGridView2.Rows.Add(row.Cells[0].Value.ToString(), row.Cells[1].Value.ToString(), row.Cells[2].Value.ToString());
                        }
                    }
                    else
                    {
                        // add row to datagridview 2
                        dataGridView2.Rows.Add(row.Cells[0].Value.ToString(), row.Cells[1].Value.ToString(), row.Cells[2].Value.ToString());
                    }
                }
            }
        }
    }
}
